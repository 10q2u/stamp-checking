﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace CS.WpfApplication.Views.Windows
{
    public partial class MyMessageBox : Window, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            CommandManager.InvalidateRequerySuggested();
        }

        private MessageBoxResult messageBoxResult = MessageBoxResult.None;

        public MessageBoxResult MessageBoxResult
        {
            get => messageBoxResult;
            set
            {
                messageBoxResult = value;
                OnPropertyChanged();
            }
        }

        private string message = "";

        public string Message
        {
            get => message;
            set
            {
                message = value;
                OnPropertyChanged();
            }
        }

        public MyMessageBox(string caption, string message)
        {
            InitializeComponent();
            DataContext = this;
            Title = caption ?? "";
            Message = message;
            MessageBoxResult = MessageBoxResult.None;
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                DragMove();
            }
        }

        public static MessageBoxResult Show(string messageBoxText, string caption, MessageBoxButton button,
            MessageBoxImage icon)
        {
            return Show(null, messageBoxText, caption, button, icon);
        }

        public static MessageBoxResult Show(string messageBoxText, string caption, MessageBoxButton button)
        {
            return Show(messageBoxText, caption, button, MessageBoxImage.None);
        }

        public static MessageBoxResult Show(string messageBoxText, string caption)
        {
            return Show(messageBoxText, caption, MessageBoxButton.OK);
        }

        public static MessageBoxResult Show(string messageBoxText)
        {
            return Show(messageBoxText, null);
        }

        public static MessageBoxResult Show(Window owner, string messageBoxText, string caption,
            MessageBoxButton button, MessageBoxImage icon)
        {
            return System.Windows.Application.Current.Dispatcher.Invoke(() =>
            {
                var window = new MyMessageBox(caption, messageBoxText)
                {
                    Owner = owner
                };

                Button buttonOk = new Button()
                    {Content = "Ок", Margin = new Thickness(8), Foreground = Brushes.White, MinWidth = 60};
                buttonOk.Click += (s, e) =>
                {
                    window.MessageBoxResult = MessageBoxResult.OK;
                    window.Close();
                };
                Button buttonCancel = new Button()
                    {Content = "Отмена", Margin = new Thickness(8), Foreground = Brushes.White, MinWidth = 100};
                buttonCancel.Click += (s, e) =>
                {
                    window.MessageBoxResult = MessageBoxResult.Cancel;
                    window.Close();
                };
                Button buttonYes = new Button()
                    {Content = "Да", Margin = new Thickness(8), Foreground = Brushes.White, MinWidth = 100};
                buttonYes.Click += (s, e) =>
                {
                    window.MessageBoxResult = MessageBoxResult.Yes;
                    window.Close();
                };
                Button buttonNo = new Button()
                    {Content = "Нет", Margin = new Thickness(8), Foreground = Brushes.White, MinWidth = 70};
                buttonNo.Click += (s, e) =>
                {
                    window.MessageBoxResult = MessageBoxResult.No;
                    window.Close();
                };

                switch (button)
                {
                    case MessageBoxButton.OK:
                        window.buttons.Children.Add(buttonOk);
                        break;

                    case MessageBoxButton.OKCancel:
                        window.buttons.Children.Add(buttonOk);
                        window.buttons.Children.Add(buttonCancel);
                        break;

                    case MessageBoxButton.YesNoCancel:
                        window.buttons.Children.Add(buttonYes);
                        window.buttons.Children.Add(buttonNo);
                        window.buttons.Children.Add(buttonCancel);
                        break;

                    case MessageBoxButton.YesNo:
                        window.buttons.Children.Add(buttonYes);
                        window.buttons.Children.Add(buttonNo);
                        break;
                }

                window.ShowDialog();
                return window.MessageBoxResult;
            });
        }

        public static MessageBoxResult Show(Window owner, string messageBoxText, string caption,
            MessageBoxButton button)
        {
            return Show(owner, messageBoxText, caption, button, MessageBoxImage.None);
        }

        public static MessageBoxResult Show(Window owner, string messageBoxText, string caption)
        {
            return Show(owner, messageBoxText, caption, MessageBoxButton.OK);
        }

        public static MessageBoxResult Show(Window owner, string messageBoxText)
        {
            return Show(owner, messageBoxText, null);
        }
    }
}