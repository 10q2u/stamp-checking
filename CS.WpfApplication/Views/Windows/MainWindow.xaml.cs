﻿using System.Windows;
using System.Windows.Input;

namespace CS.WpfApplication.Views.Windows
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void StackPanel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                DragMove();
            }
        }

        private void Image_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                System.Diagnostics.Process.Start("https://trzn.ru/");
                e.Handled = true;
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (true)
            {
                if (MyMessageBox.Show(this, "Действительно закрыть?", "Закрытие приложения",
                    MessageBoxButton.YesNoCancel) != MessageBoxResult.Yes)
                {
                    e.Cancel = true;
                }
            }
        }
    }
}