﻿using System.Windows;
using System.Windows.Input;
using CS.WpfApplication.Views.Windows;
using GalaSoft.MvvmLight.Command;

namespace CS.WpfApplication.Common
{
    public static class AppCommands
    {
        public static ICommand CloseWindowCommand => new RelayCommand<Window>(window => { window.Close(); });

        public static ICommand MinimizeWindowCommand => new RelayCommand<Window>(window =>
        {
            window.WindowState = WindowState.Minimized;
        });

        public static ICommand FAQCommand => new RelayCommand(() =>
        {
            MyMessageBox.Show("Данная программа не предназначена для использования в личных целях", "Внимание",
                MessageBoxButton.OK, MessageBoxImage.Information);
        });

        public static ICommand Copy => new RelayCommand<object>(obj =>
        {
            if (obj != null)
            {
                Clipboard.SetText(obj.ToString());
            }
        });
    }
}