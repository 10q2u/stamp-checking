﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using CS.WpfApplication.Views.Windows;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CS.WpfApplication
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private IServiceProvider ServiceProvider { get; set; }
        private IConfiguration Configuration { get; set; }

        protected override void OnStartup(StartupEventArgs e)
        {
            //TODO: Если понадобиться БД
            // IConfigurationBuilder builder = new ConfigurationBuilder()
            //     .SetBasePath(Directory.GetCurrentDirectory())
            //     .AddJsonFile("appsettings.json");
            //
            // Configuration = builder.Build();

            //Console.WriteLine(configuration.GetConnectionString("StampsDatabase"));

            // var serviceCollection = new ServiceCollection();
            // ConfigureServices(serviceCollection);
            //
            // ServiceProvider = serviceCollection.BuildServiceProvider();
            //
            // var mainWindow = ServiceProvider.GetRequiredService<MainWindow>();
        }

        private void ConfigureServices(IServiceCollection services)
        {
            // services.AddTransient(typeof(MainWindow));
        }
    }
}