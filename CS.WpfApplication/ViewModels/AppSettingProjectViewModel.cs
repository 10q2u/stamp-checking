﻿using System;
using System.Net;
using System.Net.Http;
using System.Windows;
using System.Windows.Input;
using CS.Common;
using CS.Domain;
using CS.WpfApplication.Views.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace CS.WpfApplication.ViewModels
{
    public class AppSettingProjectViewModel : ViewModelBase
    {
        private readonly FileSettingService _fileSettingService;

        public AppSettingProjectViewModel(FileSettingService fileSettingService)
        {
            _fileSettingService = fileSettingService;

            _addressYtm = _fileSettingService.LoadData().AddressYtm;
            _portYtm = _fileSettingService.LoadData().PortYtm;

            try
            {
                _fileSettingService.LoadData();
            }
            catch (Exception e)
            {
                MyMessageBox.Show($"{e}");
            }
        }

        #region Property

        #region AddressYtm : string - Адрес подключения к Утм

        /// <summary>DESCRIPTION</summary>
        private string _addressYtm;

        /// <summary>DESCRIPTION</summary>
        public string AddressYtm
        {
            get => _addressYtm;
            set => Set(ref _addressYtm, value);
        }

        #endregion

        #region PortYtm : string - Порт подключения к Утм

        /// <summary>DESCRIPTION</summary>
        private string _portYtm;

        /// <summary>DESCRIPTION</summary>
        public string PortYtm
        {
            get => _portYtm;
            set => Set(ref _portYtm, value);
        }

        #endregion

        #endregion

        #region Command

        #region CheckYTM : ICommand - Проверка УТМа

        /// <summary>DESCRIPTION</summary>
        public RelayCommand CheckYtm
        {
            get
            {
                return new RelayCommand(() =>
                {
                    var fullAddress = _fileSettingService.LoadData();
                    HttpClient httpClient = new HttpClient();

                    try
                    {
                        var result = httpClient.GetAsync(
                            $"http://{fullAddress.AddressYtm}:{fullAddress.PortYtm}/").Result;
                        if (result.StatusCode == HttpStatusCode.OK)
                            MyMessageBox.Show("Все работает");
                    }
                    catch (Exception e)
                    {
                        MyMessageBox.Show("Адрес не верный или УТМ не работает\nпроверьте подключение");
                    }
                });
            }
        }

        #endregion

        #region ApplySettings : ICommand - Применение настроек

        /// <summary>DESCRIPTION</summary>
        public ICommand ApplySettings
        {
            get
            {
                return new RelayCommand(() =>
                {
                    SaveSetting(_addressYtm, _portYtm);
                    MyMessageBox.Show("Настройки УТМа изменены");
                });
            }
        }

        #endregion

        #endregion

        #region Methods

        #region SaveSetting

        /// <summary>
        /// Метод для сохранения настроек
        /// </summary>
        /// <param name="address">Адрес УТМа</param>
        /// <param name="port">Порт УТМа</param>
        private void SaveSetting(string address, string port)
        {
            if (string.IsNullOrWhiteSpace(address) || string.IsNullOrWhiteSpace(port))
                return;

            var model = new AppSettings
            {
                AddressYtm = address,
                PortYtm = port
            };

            _fileSettingService.SaveData(model);
        }

        #endregion

        #endregion
    }
}