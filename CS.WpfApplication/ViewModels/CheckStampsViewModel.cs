﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using ClosedXML.Excel;
using CS.Common;
using CS.Domain.Models;
using CS.WpfApplication.Views.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Microsoft.Win32;
using Newtonsoft.Json;

namespace CS.WpfApplication.ViewModels
{
    public class CheckStampsViewModel : ViewModelBase
    {
        private static string _filePath = String.Empty;
        private readonly FileSettingService _fileSettingService;

        private static List<Mark> _listObjs = new List<Mark>();
        private List<string> _listCodes = new List<string>();

        public CheckStampsViewModel(FileSettingService fileSettingService)
        {
            _fileSettingService = fileSettingService;
            try
            {
                _fileSettingService.LoadData();
            }
            catch (Exception e)
            {
                MyMessageBox.Show($"{e}");
            }

            if (_listObjs != null) Items = new ObservableCollection<Mark>(_listObjs);
            LoadData();
        }

        #region Property

        #region SearchText : string - Строка поиска

        private string _searchText = "";

        public string SearchText
        {
            get => _searchText;
            set
            {
                _searchText = value;
                RaisePropertyChanged();
                _collectionView?.Refresh();
            }
        }

        #endregion

        #region Marks : ObservableCollection - Коллекция

        /// <summary>DESCRIPTION</summary>
        private ObservableCollection<Mark> _items;

        /// <summary>DESCRIPTION</summary>
        public ObservableCollection<Mark> Items
        {
            get => _items;
            set
            {
                _items = value;
                RaisePropertyChanged();
                CollectionView = CollectionViewSource.GetDefaultView(value);
                CollectionView.Filter = new Predicate<object>(item =>
                {
                    if (item is Mark model)
                    {
                        string search = SearchText?.ToLower()?.Trim() ?? "";
                        var result = (model.code.ToLower().Contains(search));

                        return result;
                    }

                    return true;
                });
            }
        }

        #endregion

        #region inforMark : string - Информация о марках

        ///<summary>$DESCRIPTOIN</summary>$
        private string _informationMarks;

        ///<summary>$DESCRIPTOIN</summary>$
        public string InformationMarks
        {
            get => _informationMarks;
            set => Set(ref _informationMarks, value);
        }

        #endregion

        #region CollectionView : ICollectionView -

        private ICollectionView _collectionView;

        public ICollectionView CollectionView
        {
            get => _collectionView;
            set
            {
                _collectionView = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #endregion

        #region Command

        #region FromFile : RelayCommand - Загрузить марки из файла

        /// <summary>DESCRIPTION</summary>
        private RelayCommand _fromFile;

        /// <summary>DESCRIPTION</summary>
        public RelayCommand FromFile
        {
            get
            {
                return _fromFile ??= new RelayCommand(() =>
                {
                    ClearAll();
                    var openFileDialog = new OpenFileDialog();
                    if (openFileDialog.ShowDialog() == true)
                    {
                        _filePath = openFileDialog.FileName;

                        openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                        openFileDialog.FilterIndex = 2;
                        openFileDialog.RestoreDirectory = true;

                        if (File.Exists(_filePath))
                        {
                            string[] readText = File.ReadAllLines(_filePath);
                            foreach (string s in readText)
                            {
                                _listCodes.Add(s);
                            }

                            MyMessageBox.Show("Файл загружен");
                        }

                        LoadData();
                    }
                    else
                    {
                        MyMessageBox.Show("Вы не загрузили файл");
                    }
                });
            }
        }

        #endregion

        #region UploadToFile : ICommnad - Выгрузить марки в файл

        /// <summary>DESCRIPTION</summary>
        private RelayCommand _uploadToFile;

        /// <summary>DESCRIPTION</summary>
        public RelayCommand UploadToFile
        {
            get
            {
                return _uploadToFile ??= new RelayCommand(() =>
                {
                    SaveFileDialog sfd = new SaveFileDialog();
                    try
                    {
                        sfd.Filter = "TXT Files (*.txt)|*.txt";
                        if (sfd.ShowDialog() == true)
                        {
                            StreamWriter sw = new StreamWriter(sfd.FileName);
                            foreach (var str in _listObjs.Where(x => x.Owner == true).Select(x => x.code))
                            {
                                sw.WriteLine(str);
                            }

                            MyMessageBox.Show("Текст записан в файл");
                            LoadData();
                            ClearAll();
                            sw.Close();
                        }
                    }
                    catch (Exception e)
                    {
                        MyMessageBox.Show($"Возникла ошибка обратитесь в ТП \r\n{e.Message}");
                    }
                });
            }
        }

        #endregion

        #region UploadToExcel : ICommnad - Выгрузить марки в Excel

        /// <summary>DESCRIPTION</summary>
        private RelayCommand _uploadToExcel;

        /// <summary>DESCRIPTION</summary>
        public RelayCommand UploadToExcel
        {
            get
            {
                return _uploadToExcel ??= new RelayCommand(() =>
                {
                    try
                    {
                        var sfd = new SaveFileDialog();
                        sfd.Filter = "Excel Files (*.xlsx)|*.xlsm";
                        if (sfd.ShowDialog() == true)
                        {
                            var workbook = new XLWorkbook();
                            var worksheet = workbook.Worksheets.Add("Лист1");

                            worksheet.Cell("A" + 1).Value = "Код";
                            worksheet.Cell("B" + 1).Value = "Статус";

                            var ws = workbook.Worksheet("Лист1");
                            worksheet.Columns("A").Width = 150; //ширина столбца по содержимому

                            var row = 1;
                            foreach (var item in _listObjs)
                            {
                                ws.Cell("A" + row.ToString()).Value = item.code;
                                ws.Cell("B" + row.ToString()).Value = item.Owner == true ? "В наличии" : "Реализовано";
                                row++;
                            }

                            workbook.SaveAs(sfd.FileName);
                            MyMessageBox.Show("Акцизки записаны в Excel");
                            ClearAll();
                            LoadData();
                        }
                    }
                    catch (Exception e)
                    {
                        MyMessageBox.Show($"Возникла ошибка обратитесь в ТП \r\n{e.Message}");
                    }
                });
            }
        }

        #endregion

        #region ClearTheTable : RelayCommand - Очистить

        /// <summary>DESCRIPTION</summary>
        public RelayCommand ClearTheTable
        {
            get
            {
                return new RelayCommand(() =>
                {
                    ClearAll();
                    LoadData();
                });
            }
        }

        #endregion

        #region SearchBrands : RelayCommand - Искать марки

        /// <summary>DESCRIPTION</summary>
        private RelayCommand _searchBrands;

        /// <summary>DESCRIPTION</summary>
        public RelayCommand SearchBrands
        {
            get
            {
                return _searchBrands ??= new RelayCommand(async () =>
                {
                    MyMessageBox.Show("Подождите пока загрузятся марки \r\nНажмите Ок");

                    var client = new HttpClient();
                    var fullAddress = _fileSettingService.LoadData();

                    if (!(string.IsNullOrEmpty(fullAddress.AddressYtm)
                          && string.IsNullOrEmpty(fullAddress.PortYtm)
                          && _listObjs.Count >= 1))
                    {
                        try
                        {
                            foreach (var item in _listCodes)
                            {
                                await SendRequest(client,
                                    $"http://" + fullAddress.AddressYtm + ":" + fullAddress.PortYtm +
                                    "/api/mark/check?code=" + item);
                                Items = new ObservableCollection<Mark>(_listObjs);
                            }

                            LoadData();
                        }
                        catch (Exception exception)
                        {
                            MyMessageBox.Show("Ошибка с марками попробуй еще раз");
                            throw;
                        }

                        MyMessageBox.Show("Марки загрузились");
                    }
                    else
                    {
                        MyMessageBox.Show("Заполните поля или файл для загрузки пустой");
                    }
                });
            }
        }

        #endregion

        #region AddNewMarksCommand : Command - Добавление марок

        /// <summary>Добавление нового stock</summary>
        private RelayCommand _addNewMarksCommand;

        /// <summary>Добавление нового stock</summary>
        public RelayCommand AddNewMarksCommand => _addNewMarksCommand
            ??= new RelayCommand(OnAddNewMarksCommandExecuted, CanAddNewMarksCommandExecute);

        /// <summary>Проверка возможности выполнения - Добавление нового stock</summary>
        private bool CanAddNewMarksCommandExecute() => true;

        /// <summary>Логика выполнения - Добавление нового stock</summary>
        private void OnAddNewMarksCommandExecuted()
        {
            var insertStampsViewModel = new InsertStampsViewModel();

            var insertStampsWindow = new InsertStampsWindow()
            {
                DataContext = insertStampsViewModel
            };

            if (insertStampsWindow.ShowDialog() == true)
                return;

            if (insertStampsViewModel.ListCodes.Count >= 1)
                _listCodes = insertStampsViewModel.ListCodes;

            LoadData();
        }

        #endregion

        #region ClearSearchCommand : Command - Очистка строки поиска

        public RelayCommand ClearSearchCommand => new RelayCommand(() => { SearchText = ""; });

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Очистка
        /// </summary>
        private void ClearAll()
        {
            _listObjs = new List<Mark>();
            _listCodes = new List<string>();
            Items = new ObservableCollection<Mark>();
        }


        private void LoadData()
        {
            var countFalse = _listObjs.Count(x => x.Owner == false);
            var countTrue = _listObjs.Count(x => x.Owner == true);
            InformationMarks =
                $" Всего {_listCodes.Count} Количество марок на остатках: {countTrue} Марки которые не числятся за организацией {countFalse} ";
        }

        /// <summary>
        /// Отправка запроса на сервер
        /// </summary>
        /// <param name="client"></param>
        /// <param name="url"></param>
        private async Task SendRequest(HttpClient client, string url)
        {
            var response = await client.GetAsync(url);
            var str = await response.Content.ReadAsStringAsync();

            // var jsonData =
            //     "{\"code\":\"136300040395551018001JRQLALMI4GRZANQODTAL3TAZE47ZEZOKIMM5V6PNG2HFK6ZUJVALWPEQQDZNDU5RZVSGLY6NTWF4RYLMY4U3UKDGXJAYPTJ3Z3FOOOHCZWXHRDBV2BUAJPWZDTQ5RCEPI\"," +
            //     "\"Owner\":true}";

            var jsonMark = JsonConvert.DeserializeObject<Mark>(str);
            _listObjs.Add(jsonMark);
        }

        #endregion
    }
}