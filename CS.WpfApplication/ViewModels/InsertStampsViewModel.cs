﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using CS.WpfApplication.Views.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace CS.WpfApplication.ViewModels
{
    public class InsertStampsViewModel : ViewModelBase
    {
        public List<string> ListCodes = new List<string>();

        #region Property

        #region SingleMark : string - Одиночкая марка

        /// <summary>DESCRIPTION</summary>
        private string _singleMark = string.Empty;

        /// <summary>DESCRIPTION</summary>
        public string SingleMark
        {
            get => _singleMark;
            set => Set(ref _singleMark, value);
        }

        #endregion

        #endregion

        #region Command

        #region UploadBrand : RelayCommand - Искать по одной марки

        /// <summary>DESCRIPTION</summary>
        private RelayCommand _uploadBrand;

        /// <summary>DESCRIPTION</summary>
        public RelayCommand UploadBrand
        {
            get
            {
                return _uploadBrand ??= new RelayCommand(() =>
                {
                    if (string.IsNullOrEmpty(SingleMark))
                        MyMessageBox.Show("Пустые поля");

                    string[] strMark = SingleMark.Split(' ', '@', ',', '!');
                    foreach (var str in strMark)
                    {
                        ListCodes.Add(str);
                    }

                    MyMessageBox.Show("Теперь нажмите на кнопку: Отменить");
                    MessengerInstance.Send<NotificationMessage>(new NotificationMessage("123"));
                });
            }
        }

        #endregion

        #endregion
    }
}