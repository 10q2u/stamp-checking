﻿using CS.Common;
using Microsoft.Extensions.DependencyInjection;

namespace CS.WpfApplication.ViewModels
{
    public class ViewModelLocator
    {
        private static ServiceProvider _serviceProvider;

        public ViewModelLocator()
        {
            ServiceCollection services = new ServiceCollection();

            //Windows
            services.AddSingleton<MainWindowViewModel>();
            services.AddSingleton<LoaderViewModel>();

            //User Controlls
            services.AddSingleton<CheckStampsViewModel>();
            services.AddSingleton<AppSettingProjectViewModel>();
            services.AddSingleton<InformationStartPageViewModel>();
            
            services.AddSingleton<FileSettingService>(_ => new FileSettingService("Setting.json"));

            _serviceProvider = services.BuildServiceProvider();
        }

        public static MainWindowViewModel MainWindowViewModel =>
            (MainWindowViewModel)_serviceProvider.GetService(typeof(MainWindowViewModel));

        public static LoaderViewModel LoaderViewModel =>
            (LoaderViewModel)_serviceProvider.GetService(typeof(LoaderViewModel));

        public static CheckStampsViewModel CheckStampsViewModel =>
            (CheckStampsViewModel)_serviceProvider.GetService(typeof(CheckStampsViewModel));

        public static AppSettingProjectViewModel AppSettingProjectViewModel =>
            (AppSettingProjectViewModel)_serviceProvider.GetService(typeof(AppSettingProjectViewModel));

        public static InsertStampsViewModel InsertStampsViewModel =>
            (InsertStampsViewModel)_serviceProvider.GetService(typeof(InsertStampsViewModel));
        
        public static InformationStartPageViewModel InformationStartPageViewModel =>
            (InformationStartPageViewModel)_serviceProvider.GetService(typeof(InformationStartPageViewModel));

    }
}