﻿using System.Windows;
using System.Windows.Input;
using CS.Domain.Models;
using CS.WpfApplication.Views.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace CS.WpfApplication.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        public MainWindowViewModel()
        {
            CurrentModel = ViewModelLocator.AppSettingProjectViewModel;
        }

        #region Command

        #region CurrentModel : ViewModel - Текущая дочерняя модель-представления

        /// <summary>Текущая дочерняя модель-представления</summary>
        private ViewModelBase _CurrentModel;

        /// <summary>Текущая дочерняя модель-представления</summary>
        public ViewModelBase CurrentModel
        {
            get => _CurrentModel;
            private set => Set(ref _CurrentModel, value);
        }

        #endregion

        #region Command ShowCheckingStampsViewCommand - Отобразить представление Checking Stamps

        /// <summary>Отобразить представление</summary>
        private RelayCommand _ShowCheckingStampsViewCommand;

        /// <summary>Отобразить представление Check Stamps</summary>
        public RelayCommand ShowCheckingStampsViewCommand => _ShowCheckingStampsViewCommand
            ??= new RelayCommand(OnShowCheckingStampsViewCommandExecuted, CanShowCheckingStampsViewCommandExecute);

        private bool CanShowCheckingStampsViewCommandExecute() => true;

        private void OnShowCheckingStampsViewCommandExecuted()
        {
            CurrentModel = ViewModelLocator.CheckStampsViewModel;
        }

        #endregion

        #region NewIdea : ICommnad - Идеи

        /// <summary>DESCRIPTION</summary>
        private RelayCommand _newIdea;

        /// <summary>DESCRIPTION</summary>
        public RelayCommand NewIdea
        {
            get
            {
                return _newIdea ??= new RelayCommand(() =>
                {
                    MyMessageBox.Show(
                        "Если есть какие-то идеи или пожелания, то обратитесь в ТП");
                });
            }
        }

        #endregion

        #region Command ShowInformationStartPageViewModelCommand - FAQ

        /// <summary>Отобразить представление</summary>
        private RelayCommand _showInformationStartPageViewModelCommand;

        /// <summary>Отобразить представление App Setting Project</summary>
        public RelayCommand ShowInformationStartPageViewModelCommand => _showInformationStartPageViewModelCommand
            ??= new RelayCommand(OnShowInformationStartPageViewModelCommandExecuted, CanShowInformationStartPageViewModelCommandExecute);

        private bool CanShowInformationStartPageViewModelCommandExecute() => true;

        private void OnShowInformationStartPageViewModelCommandExecuted()
        {
            CurrentModel = ViewModelLocator.InformationStartPageViewModel;
        }

        #endregion
        
        
        #region Command ShowAppSettingViewCommand - Настройки

        /// <summary>Отобразить представление</summary>
        private RelayCommand _showAppSettingViewCommand;

        /// <summary>Отобразить представление App Setting Project</summary>
        public RelayCommand ShowAppSettingViewCommand => _showAppSettingViewCommand
            ??= new RelayCommand(OnShowAppSettingViewCommandExecuted, CanShowAppSettingViewCommandExecute);

        private bool CanShowAppSettingViewCommandExecute() => true;

        private void OnShowAppSettingViewCommandExecuted()
        {
            CurrentModel = ViewModelLocator.AppSettingProjectViewModel;
        }

        #endregion

        #endregion
    }
}