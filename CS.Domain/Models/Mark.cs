﻿using Newtonsoft.Json;

namespace CS.Domain.Models
{
    public class Mark
    {
        [JsonProperty(PropertyName = "code")]
        public string code { get; set; }

        [JsonProperty(PropertyName = "owner")]
        public bool? Owner { get; set; }
    }
}