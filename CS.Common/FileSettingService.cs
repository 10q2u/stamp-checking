﻿using System;
using System.IO;
using System.Net.Http.Json;
using CS.Domain;
using Newtonsoft.Json;

namespace CS.Common
{
    public class FileSettingService
    {
        private readonly string _path;

        public FileSettingService(string path)
        {
            _path = path;
        }

        public void SaveData(AppSettings model)
        {
            using (var streamWriter = File.CreateText(_path))
            {
                var output = JsonConvert.SerializeObject(model);
                streamWriter.Write(output);
            }
        }

        public AppSettings LoadData()
        {
            var fileExists = File.Exists(_path);
            if (!fileExists)
            {
                File.CreateText(_path).Dispose();
                SaveData(new AppSettings
                {
                    AddressYtm = "localhost",
                    PortYtm = "8080"
                });
            }

            using (var reader = File.OpenText(_path))
            {
                var fieText = reader.ReadToEnd();
                return JsonConvert.DeserializeObject<AppSettings>(fieText);
            }
        }
    }
}